# Dotfiles

Configuration details for certain environments (shell, vim, etc.)

Initial setup of dotfiles following the course at [dotfiles.eieio.xyz](http://dotfiles/eieio.xyz). -  this was moved to dotbot branch for posterity.I am now using chezmoi

## Why chezmoi ?

I have multiple machines, and I need flexibility for handling multiple OS (Windows, Mac, Linux, BSD). While also, not repeating myself on common utils (nvim, tmux, zsh, vscode, etc.)

## TODO
- Terminal Preferences
- Changed Shell to ZSH
- Dock Preferences
- Mission COntrol Prefernces 
- Finder Show Path Bar
- trackpad (Three finger drag, and tap to click)
- .zshrc
- Git (config and SSH)
- Refactor to include suggestions from Travis (he knows who he is)
- Refactor to allow for multiple machines/endpoints (Linux, FreeBSD)
- Add Vim config, etc.
